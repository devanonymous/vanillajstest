const WebSocketServer = new require('ws');

const connections = {};

const webSocketServer = new WebSocketServer.Server({
    port: 8081
});

console.log('server is listening 8081 port');

const json = JSON.stringify({
        "_id": "5c92cb91b41c91718aa5eadf",
        "index": 0,
        "guid": "e9151d54-9704-4646-8b9b-f1c196d36b03",
        "isActive": true,
        "balance": "$3,651.47",
        "picture": "http://placehold.it/32x32",
        "age": 28,
        "eyeColor": "green",
        "name": "Pamela Frost",
        "gender": "female",
        "company": "KYAGORO",
        "email": "pamelafrost@kyagoro.com",
        "phone": "+1 (843) 519-3412",
        "address": "216 Bleecker Street, Silkworth, Nevada, 2764",
        "about": "Est irure est esse enim fugiat officia aliqua. Deserunt quis duis dolore qui pariatur veniam in occaecat eu sit do. Commodo dolor mollit aliqua ea magna enim nulla consectetur quis cillum non. Incididunt officia reprehenderit occaecat exercitation nostrud est irure sit cupidatat anim officia eiusmod.\r\n",
        "registered": "2017-01-31T11:58:19 -03:00",
        "latitude": -50.723236,
        "longitude": 40.528918,
        "tags": [
            "reprehenderit",
            "pariatur",
            "irure",
            "et",
            "id",
            "fugiat",
            "adipisicing"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Charity Small"
            },
            {
                "id": 1,
                "name": "Marcie Conrad"
            },
            {
                "id": 2,
                "name": "Celia Barker"
            }
        ],
        "greeting": "Hello, Pamela Frost! You have 6 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "5c92cb91cae19aba5493946b",
        "index": 1,
        "guid": "7e8a4fe2-ed6d-43e6-a33d-0630ea31cf45",
        "isActive": true,
        "balance": "$3,800.76",
        "picture": "http://placehold.it/32x32",
        "age": 24,
        "eyeColor": "green",
        "name": "Wynn Sandoval",
        "gender": "male",
        "company": "LOTRON",
        "email": "wynnsandoval@lotron.com",
        "phone": "+1 (959) 440-3798",
        "address": "288 Jackson Court, Deercroft, Oklahoma, 3267",
        "about": "Reprehenderit eu labore consequat cillum commodo elit adipisicing duis. Irure fugiat nisi fugiat dolor tempor dolor do Lorem reprehenderit cupidatat enim duis ut. Do sint in occaecat magna non do deserunt aliquip velit minim aute. Cillum consectetur ea velit deserunt enim. Labore tempor velit ex eiusmod dolore deserunt ut ut labore.\r\n",
        "registered": "2019-02-28T11:08:16 -03:00",
        "latitude": -0.314204,
        "longitude": -162.894951,
        "tags": [
            "pariatur",
            "officia",
            "aliqua",
            "in",
            "esse",
            "dolor",
            "duis"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Marisol Nguyen"
            },
            {
                "id": 1,
                "name": "Miles Ferguson"
            },
            {
                "id": 2,
                "name": "Erin Wilkins"
            }
        ],
        "greeting": "Hello, Wynn Sandoval! You have 5 unread messages.",
        "favoriteFruit": "banana"
    },
    {
        "_id": "5c92cb91bbb0d68877a11f3b",
        "index": 2,
        "guid": "3a5f3b08-976b-4d78-9449-8424631eeecd",
        "isActive": true,
        "balance": "$1,637.01",
        "picture": "http://placehold.it/32x32",
        "age": 32,
        "eyeColor": "blue",
        "name": "Aurora Mccarty",
        "gender": "female",
        "company": "COWTOWN",
        "email": "auroramccarty@cowtown.com",
        "phone": "+1 (882) 432-2930",
        "address": "221 Pooles Lane, Leroy, Minnesota, 3426",
        "about": "Quis dolore non non aute tempor ipsum reprehenderit est culpa. Fugiat sit ex nisi consectetur occaecat culpa ad aliquip anim do anim ad aute. Eiusmod adipisicing aliquip elit aute duis. Sunt ea ullamco do reprehenderit voluptate culpa velit.\r\n",
        "registered": "2017-05-15T10:34:52 -03:00",
        "latitude": 34.022681,
        "longitude": -102.723794,
        "tags": [
            "occaecat",
            "consectetur",
            "exercitation",
            "reprehenderit",
            "cillum",
            "reprehenderit",
            "voluptate"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Laurel Patel"
            },
            {
                "id": 1,
                "name": "Frederick Byrd"
            },
            {
                "id": 2,
                "name": "Joy Strickland"
            }
        ],
        "greeting": "Hello, Aurora Mccarty! You have 7 unread messages.",
        "favoriteFruit": "banana"
    },
    {
        "_id": "5c92cb91861999ed122cd5fb",
        "index": 3,
        "guid": "999a578f-2b8b-4474-b507-cc1d957edfb4",
        "isActive": true,
        "balance": "$2,982.29",
        "picture": "http://placehold.it/32x32",
        "age": 33,
        "eyeColor": "blue",
        "name": "Nancy Finch",
        "gender": "female",
        "company": "ACUMENTOR",
        "email": "nancyfinch@acumentor.com",
        "phone": "+1 (906) 509-3097",
        "address": "679 Bennet Court, Grimsley, Alaska, 4337",
        "about": "Ipsum excepteur laboris laborum voluptate esse. Commodo aliqua anim adipisicing et nulla excepteur incididunt adipisicing laboris adipisicing. Veniam et dolor Lorem officia elit. Ipsum sunt exercitation non do do. Est tempor ad qui deserunt quis duis labore culpa irure sit occaecat sunt amet. Sit officia fugiat dolore laborum aliqua consectetur cupidatat ut duis deserunt ex. Velit esse culpa deserunt nulla.\r\n",
        "registered": "2014-10-11T08:41:00 -04:00",
        "latitude": -3.803268,
        "longitude": -178.971391,
        "tags": [
            "tempor",
            "anim",
            "occaecat",
            "exercitation",
            "laborum",
            "Lorem",
            "incididunt"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Flowers Mueller"
            },
            {
                "id": 1,
                "name": "Hamilton Mcknight"
            },
            {
                "id": 2,
                "name": "Ester Mcguire"
            }
        ],
        "greeting": "Hello, Nancy Finch! You have 9 unread messages.",
        "favoriteFruit": "apple"
    },
    {
        "_id": "5c92cb91c874365638728225",
        "index": 4,
        "guid": "c9b709af-ba03-42ed-b3e5-4c2edc63a714",
        "isActive": false,
        "balance": "$1,489.06",
        "picture": "http://placehold.it/32x32",
        "age": 21,
        "eyeColor": "brown",
        "name": "Norris Walton",
        "gender": "male",
        "company": "DOGTOWN",
        "email": "norriswalton@dogtown.com",
        "phone": "+1 (859) 429-2448",
        "address": "412 Bushwick Avenue, Lopezo, Rhode Island, 9965",
        "about": "Minim elit pariatur ea incididunt nisi reprehenderit pariatur nostrud. Aliqua magna pariatur ex exercitation reprehenderit elit aliquip cillum amet Lorem cupidatat id et irure. Lorem qui ad commodo dolor anim pariatur ex eiusmod. Voluptate consectetur sunt velit ad pariatur laboris. Qui duis eiusmod culpa id excepteur minim ex et proident mollit incididunt.\r\n",
        "registered": "2017-08-19T12:23:37 -03:00",
        "latitude": 65.841342,
        "longitude": 115.863007,
        "tags": [
            "ut",
            "magna",
            "ipsum",
            "aute",
            "ullamco",
            "aliquip",
            "esse"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Mabel Morales"
            },
            {
                "id": 1,
                "name": "Emerson Keller"
            },
            {
                "id": 2,
                "name": "Elinor Howe"
            }
        ],
        "greeting": "Hello, Norris Walton! You have 1 unread messages.",
        "favoriteFruit": "apple"
    });

webSocketServer.on('connection', ws => {

    const id = Math.random();
    connections[id] = ws;
    console.log("new connection " + id);

    ws.on('message', function(message) {
        console.log('received a new message ' + message);

        for (let key in connections) {
            connections[key].send(json);
        }
    });

    ws.on('close', function() {
        console.log('connection closed ' + id);
        delete connections[id];
    });

});