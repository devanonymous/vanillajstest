import Swiper from 'swiper';
import commonJs from './module2'

/* 01 */
const mySwiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

/* 02 */
const drawFigures = () => {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.arc(700, 50, 40, 0, 2 * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(95, 60); // первая внешняя точка
    ctx.lineTo(60, 20); //вторая внешняя точка
    ctx.lineTo(25, 60); //третья внешняя точка
    ctx.closePath();
    ctx.stroke();

    ctx.rect(35, 490, 100, 100);
    ctx.stroke();

    ctx.rect(630, 490, 150, 100);
    ctx.stroke();
};

drawFigures();

/* 03 */
require('./module1');
commonJs();

/* 04 */
class BMW {
    constructor() {
        this.speed = 220
    }
}

class Audi {
    constructor() {
        this.speed = 240
    }
}

class Lada {
    constructor() {
        this.speed = 80
    }
}

class NoName {
    constructor() {
        this.speed = 80
    }
}

class CarsFactory {
    create(model) {
        let car = null;
        switch (model) {
            case "BMW":
                car = new BMW();
                break;
            case "Audi":
                car = new Audi();
                break;
            case "Lada":
                car = new Lada();
                break;
            default:
                car = new NoName();
        }
        car.model = model;
        return car;
    }
}

const factory = new CarsFactory();
const bimer = factory.create("BMV");
const audi = factory.create("Audi");
const taz = factory.create("Lada");

/* 05 */


const socket = new WebSocket('ws://localhost:8081');

socket.addEventListener('open', (event) => {
    socket.send('Give me Json!');
});

socket.addEventListener('message', (event) => {
    // console.log('json recieved ', event.data);
    document.getElementById("socket_json").innerText = event.data;
});

/* 06 */
const fetchJson = async () => {
    const response = await fetch('http://localhost:3003/', {
        method: "GET",
        headers: new Headers({
            "Content-Type": "application/json"
        })
    });
    if (response.ok) {
        const jsonres = await response.json();
        document.getElementById("fetch_json").innerText = jsonres
    } else {
        document.getElementById("fetch_json").innerText = "error " + response.error()
    }
    console.log("fetch")
};

fetchJson();

/*  07  */
/*
* смотреть в dist/center2.html
*
* */


/*  08  */

document.getElementById("open_close_button").addEventListener("click", () => {
    setTimeout(() => {
        document.getElementById("first_el").classList.toggle("disabled");
        document.getElementById("second_el").classList.toggle("disabled");
        document.getElementById("third_el").classList.toggle("disabled");
    }, 1000)
});

/* 09 */
const generateRandomColor = () => {

    const r=Math.floor(Math.random() * (256));

    const g=Math.floor(Math.random() * (256));

    const b=Math.floor(Math.random() * (256));

    const color = '#' + r.toString(16) + g.toString(16) + b.toString(16);

    return color;


};



const squaresDiv = document.getElementById("squares");

const max = 100;
const min = 10;

const squaresCounts = Math.floor(Math.random() * (max - min)) + min;
document.getElementById("counter").innerText = `сгенерировано ${squaresCounts} квадратиков`;

for (let i = 0; i < squaresCounts; i++) {
    const el = document.createElement('div');
    el.innerHTML =  `<div class="random_square"></div>`;
    el.firstChild.style.backgroundColor = generateRandomColor();
    console.log(generateRandomColor());
    squaresDiv.appendChild(el.firstChild);
}